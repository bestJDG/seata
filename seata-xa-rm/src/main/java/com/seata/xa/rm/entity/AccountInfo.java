package com.seata.xa.rm.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author jideguang
 * @Date 2024年01月15日 14:07
 */
@Data
public class AccountInfo implements Serializable {
    private Long id;
    private String accountName;
    private String accountNo;
    private String accountPassword;
    private Double accountBalance;

}

