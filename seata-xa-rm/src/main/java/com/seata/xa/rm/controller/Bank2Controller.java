package com.seata.xa.rm.controller;

import com.seata.xa.rm.dao.AccountInfoDao;
import io.seata.core.context.RootContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author jideguang
 * @Date 2024年01月15日 14:04
 */
@RestController
public class Bank2Controller {
    @Autowired
    private AccountInfoDao accountInfoDao;

    @RequestMapping("/transfer")
    public Boolean transfer(@RequestParam("amount") Double amount) {
        System.out.println("bank2=============="+ RootContext.getXID());
        this.accountInfoDao.updateAccountBalance("2", amount);
        int i = 10 / 0;
        return true;
    }

}

