package com.seata.xa.tm.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author jideguang
 * @Date 2024年01月15日 13:51
 */
@Data
public class AccountInfo implements Serializable {
    private Long id;
    private String accountName;
    private String accountNo;
    private String accountPassword;
    private Double accountBalance;

}

