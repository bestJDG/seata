package com.seata.xa.tm.controller;

import com.seata.xa.tm.dao.AccountInfoDao;
import com.seata.xa.tm.feignClient.Bank2Client;
import io.seata.core.context.RootContext;
import io.seata.core.exception.TransactionException;
import io.seata.spring.annotation.GlobalTransactional;
import io.seata.tm.api.GlobalTransactionContext;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description TODO
 * @Author jideguang
 * @Date 2024年01月15日 13:50
 */
@RestController
public class Bank1Controller {

    @Autowired
    private Bank2Client bank2Client;

    @Autowired
    private AccountInfoDao accountInfoDao;

    @RequestMapping("/transfer")
    @GlobalTransactional(rollbackFor = Exception.class)
    public String test(@RequestParam("amount") Double amount) throws TransactionException {
        System.out.println("bank1=============="+RootContext.getXID());
        this.accountInfoDao.updateAccountBalance("1", amount*(-1));
        Boolean transfer = bank2Client.transfer(amount);
        if(!transfer){
            GlobalTransactionContext.reload(RootContext.getXID()).rollback();
        }
        System.out.println(transfer);
        return "bank1向bank2转账:" + amount;
    }
}
