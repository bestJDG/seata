package com.seata.xa.tm.feignClient;

import org.springframework.stereotype.Component;

/**
 * @Description TODO
 * @Author jideguang
 * @Date 2024年02月07日 11:41
 */
@Component
public class Bank2ClientFallBack implements Bank2Client {
    @Override
    public Boolean transfer(Double amount) {
        return false;
    }
}
