package com.seata.xa.tm.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description TODO
 * @Author jideguang
 * @Date 2024年01月15日 13:56
 */
@FeignClient(value = "seata-xa-rm-service",fallback = Bank2ClientFallBack.class)
@Component
public interface Bank2Client {

    @GetMapping("/transfer")
    Boolean transfer(@RequestParam("amount") Double amount);
}
